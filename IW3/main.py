## -*- coding: utf-8 -*-
import sys
from json import dump, load
from secrets import token_bytes

from PyQt5.QtWidgets import *
from des import DesKey
from pygost.gost28147 import ecb_encrypt, ecb_decrypt

# pip install pygost
# pip install des
# pip install PyQt5

task = """5. Реализовать программный продукт, позволяющий шифровать и
расшифровывать сообщения на русском и английском языках с помощью
двух блочных симметричных шифров(DES, MAGMA). Чтение открытого
текста и шифртекста должно быть возможно с клавиатуры и из файла, запись
результата шифрования/расшифрования возможна на экран и в файл. Ключ
формируется автоматически и сохраняется на весь сеанс шифрования. Ключ
сохраняется в отдельный файл. Возможно хранение нескольких ключей от
разных сеансов шифрования. У пользователя есть возможность выбора,
какой криптосистемой пользоваться. Шифрование реализовать для двух
различных кодировок текста. Для реализации криптоалгоритмов
пользоваться встроенными библиотеками используемых языков"""


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.mainWidget = QWidget()
        self.setCentralWidget(self.mainWidget)
        self.source_text_widget = QLineEdit()
        self.result_widget = QLineEdit()
        self.radio_button_des = QRadioButton('DES')
        self.radio_button_magma = QRadioButton('MAGMA')
        self.radio_button_windows = QRadioButton('windows1251')
        self.radio_button_ibm = QRadioButton('IBM866')
        self.cypher_button_group = QButtonGroup()
        self.code_button_group = QButtonGroup()
        self.key_des_token = None
        self.key_des = None
        self.key_magma = None
        self.code = 'windows-1251'
        self.initUI()

    def initUI(self):
        grid = QGridLayout()

        self.source_text_widget.setMinimumWidth(300)
        self.result_widget.setReadOnly(True)

        self.radio_button_des.setChecked(True)
        self.radio_button_windows.setChecked(True)

        self.radio_button_windows.toggled.connect(self.set_windows)
        self.radio_button_ibm.toggled.connect(self.set_ibm)

        self.cypher_button_group.addButton(self.radio_button_des)
        self.cypher_button_group.addButton(self.radio_button_magma)
        self.code_button_group.addButton(self.radio_button_windows)
        self.code_button_group.addButton(self.radio_button_ibm)

        button_encrypt = QPushButton("Шифровать")
        button_decrypt = QPushButton("Расшифровать")
        button_open_keys = QPushButton("Открыть ключи")
        button_save_keys = QPushButton("Сохранить ключи")
        button_save_text = QPushButton("Сохранить текст")
        button_open_text = QPushButton("Открыть текст")
        button_encrypt.clicked.connect(self.go_encrypt)
        button_decrypt.clicked.connect(self.go_decrypt)
        button_save_keys.clicked.connect(self.save_keys)
        button_open_keys.clicked.connect(self.open_keys)
        button_open_text.clicked.connect(self.open_text)
        button_save_text.clicked.connect(self.save_text)

        self.mainWidget.setLayout(grid)
        grid.addWidget(QLabel("Исходный текст:"), 1, 0)
        grid.addWidget(self.source_text_widget, 1, 1)
        grid.addWidget(button_open_text, 1, 3)
        grid.addWidget(button_save_text, 2, 3)

        grid.addWidget(QLabel("Результат:"), 2, 0)
        grid.addWidget(self.result_widget, 2, 1)

        buttons = QWidget()
        layout = QGridLayout(buttons)
        layout.addWidget(button_encrypt, 0, 0)
        layout.addWidget(button_decrypt, 0, 1)
        layout.addWidget(button_open_keys, 1, 0)
        layout.addWidget(button_save_keys, 1, 1)

        grid.addWidget(buttons, 3, 1)

        radio_crypt = QWidget()
        radio_layout = QVBoxLayout(radio_crypt)
        radio_layout.addWidget(self.radio_button_des)
        radio_layout.addWidget(self.radio_button_magma)
        grid.addWidget(radio_crypt, 3, 3)

        radio_code = QWidget()
        radio_code_layout = QVBoxLayout(radio_code)
        radio_code_layout.addWidget(self.radio_button_windows)
        radio_code_layout.addWidget(self.radio_button_ibm)
        grid.addWidget(radio_code, 3, 0)

    def go_encrypt(self):
        indexOfChecked = [self.cypher_button_group.buttons()[x].isChecked() for x in
                          range(len(self.cypher_button_group.buttons()))].index(True)
        if indexOfChecked == 0:
            self.go_des_en()
        else:
            self.go_magma_en()

    def go_des_en(self):
        if self.key_des is None:
            self.key_des_token = token_bytes(8)
            self.key_des = DesKey(self.key_des_token)
        input_text = self.source_text_widget.text()
        if not input_text:
            return
        res = self.key_des.encrypt(input_text.encode(self.code), padding=True)
        self.result_widget.setText(res.decode(self.code))
        print()

    def set_windows(self):
        self.code = 'windows-1251'

    def set_ibm(self):
        self.code = 'IBM866'

    def go_decrypt(self):
        indexOfChecked = [self.cypher_button_group.buttons()[x].isChecked() for x in
                          range(len(self.cypher_button_group.buttons()))].index(True)
        if indexOfChecked == 0:
            self.go_des_de()
        else:
            self.go_magma_de()

    def go_des_de(self):
        if self.key_des is None:
            print("need to open key")
        input_text = self.source_text_widget.text()
        if not input_text:
            return
        res = self.key_des.decrypt(input_text.encode(self.code), padding=True)
        self.result_widget.setText(res.decode(self.code))

    def go_magma_en(self):
        if self.key_magma is None:
            self.key_magma = token_bytes(32)
        input_text = self.source_text_widget.text()
        if not input_text:
            return
        temp = input_text.encode(self.code)
        while len(temp) % 8 != 0:
            input_text += " "
            temp = input_text.encode(self.code)

        res = ecb_encrypt(self.key_magma, temp)
        self.result_widget.setText(res.decode(self.code))

    def go_magma_de(self):
        if self.key_magma is None:
            print("need to open key")
        input_text = self.source_text_widget.text()
        if not input_text:
            return
        temp = input_text.encode(self.code)
        res = ecb_decrypt(self.key_magma, temp)
        self.result_widget.setText(res.decode(self.code))

    def save_keys(self):
        path, _ = QFileDialog.getSaveFileName(self, 'Save Keys', filter="json(*.json)")
        if not path:
            return
        if self.key_magma is not None:
            magma = self.key_magma.decode(self.code)
        else:
            magma = None
        if self.key_des_token is not None:
            des = self.key_des_token.decode(self.code)
        else:
            des = None
        with open(path, "w", encoding="cp1251") as file:
            dump({
                'magma': magma,
                'des': des,
                'code': self.code
            }, file, indent=4)

    def open_keys(self):
        path, _ = QFileDialog.getOpenFileName(self, 'Open Keys', filter="json(*.json)")
        if not path:
            return
        with open(path, encoding='cp1251') as file:
            data = load(file)
            code = data["code"]
            if data["des"] is not None:
                self.key_des_token = data["des"].encode(code)

            self.key_des = DesKey(self.key_des_token)
            if data["magma"] is not None:
                self.key_magma = data["magma"].encode(code)

    def save_text(self):
        path, _ = QFileDialog.getSaveFileName(self, 'Save File', filter="txt(*.txt)")
        if not path:
            return
        with open(path, "w", encoding=self.code) as file:
            file.write(self.result_widget.text())

    def open_text(self):
        path, _ = QFileDialog.getOpenFileName(self, 'Open Text', filter="txt(*.txt)")
        if not path:
            return
        with open(path, "r", encoding=self.code) as file:
            self.source_text_widget.setText(file.read())


if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = MainWindow()
    ex.show()
    sys.exit(app.exec_())
