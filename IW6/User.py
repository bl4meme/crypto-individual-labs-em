from secrets import token_hex
from time import time

from KDC import KDC
from des import DesKey


class User:
    def __init__(self, name: str):
        self.code = "IBM866"
        self.name = name
        self.id = None
        self.last_nonce = None
        self.password = None
        self.user_key = token_hex(8).encode(self.code)
        self.database = {}

    def set_id(self, id, password):
        self.id = id
        self.password = password

    def get_session_key_from_kdc(self, message: str, kdc: KDC):
        """
            шаг 1 - шаг 3
            возвращаются результаты шага 3
        """
        nonce_rB = self.get_user_nonce()
        alice_id = message[:32]
        bob_id = message[32:64]
        nonce_r = message[64:74]
        ticket_alice = message[74:]
        ticket_message = str(alice_id) + str(bob_id) + str(nonce_r) + str(nonce_rB)
        ticket_bob = DesKey(self.user_key).encrypt(ticket_message.encode(self.code), padding=True).decode(self.code)
        return kdc.read_tickets(ticket_alice + ticket_bob, alice_id, bob_id)

    def get_session_key(self, message, user):
        """
            обработка билетов от KDC
            сохраняет сессионный ключ
        """
        decrypted_message = DesKey(self.user_key).decrypt(message, padding=True).decode(self.code)
        user_r = decrypted_message[:10]
        self.save_session_key(user, decrypted_message[10:])

    def save_session_key(self, user, key):
        self.database.update({user: key})

    def get_user_nonce(self):
        self.last_nonce = int(time())
        return self.last_nonce

    def test_send(self, bob):
        message = "test_message"
        encrypted_message = DesKey(self.database[bob.id].encode(self.code)).encrypt(message.encode(self.code),
                                                                                    padding=True)
        decrypted_message = DesKey(bob.database[self.id].encode(self.code)).decrypt(encrypted_message,
                                                                                    padding=True).decode(self.code)
        # проверка корректной расшифровки (сессионных ключёй)
        assert message == decrypted_message
        print("Успешный обмен ключами")
