import hashlib
from random import choice
from secrets import token_hex
from string import ascii_lowercase

from des import DesKey


class KDC:
    def __init__(self):
        self.code = "IBM866"
        self.database = {}
        self.last_user_id = 0

    @staticmethod
    def random_string():
        return ''.join(choice(ascii_lowercase) for i in range(10))

    def register(self, key):
        self.last_user_id += 1
        hash_id = hashlib.md5(self.last_user_id.to_bytes(2, 'big')).hexdigest()
        password = self.random_string()
        self.database.update({hash_id:
                                  {"password": password,
                                   "user_key": key,
                                   "usr_kdc_key": self.get_key(8)}
                              })
        return hash_id, password

    def read_tickets(self, message, alice_id, bob_id):
        alice_ticket, bob_ticket = message[:88], message[88:]
        alice_key = self.database.get(alice_id)["user_key"]
        bob_key = self.database.get(bob_id)["user_key"]
        alice_message = DesKey(alice_key).decrypt(alice_ticket.encode(self.code), padding=True)
        bob_message = DesKey(bob_key).decrypt(bob_ticket.encode(self.code), padding=True)
        r = alice_message[-20:-10]
        rA = alice_message[-10:]
        rB = bob_message[-10:]
        # проверка совпадаения общей метки времени
        assert alice_message[-20:-10] == bob_message[-20:-10]
        session_key = self.get_key(8)
        bob_ticket = DesKey(bob_key).encrypt(rB + session_key, padding=True)
        alice_ticket = DesKey(alice_key).encrypt(rA + session_key, padding=True)
        message = r + bob_ticket + alice_ticket
        return message

    def get_key(self, count):
        return token_hex(count).encode(self.code)
