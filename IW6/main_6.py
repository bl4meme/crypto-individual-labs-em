from time import time

from KDC import KDC
from User import User
from des import DesKey

code = "IBM866"
kdc = KDC()
alice = User("Alice")
bob = User("Bob")

# регистрация в KDC
alice.set_id(*kdc.register(alice.user_key))
bob.set_id(*kdc.register(bob.user_key))

# начало обмена
nonce_r = int(time())
nonce_rA = alice.get_user_nonce()
ticket_message = str(alice.id) + str(bob.id) + str(nonce_r) + str(nonce_rA)
ticket = DesKey(alice.user_key).encrypt(ticket_message.encode(code), padding=True)
message = str(alice.id) + str(bob.id) + str(nonce_r) + ticket.decode(code)
step_3_result = bob.get_session_key_from_kdc(message, kdc)


bob.get_session_key(step_3_result[10: (len(step_3_result) - 10) // 2 + 10], alice.id)
alice.get_session_key(step_3_result[(len(step_3_result) - 10) // 2 + 10:], bob.id)

alice.test_send(bob)
