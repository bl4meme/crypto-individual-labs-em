import sys
from PyQt5.QtWidgets import *
from json import dump, load
from math import gcd
from random import randint

alphabet = '-abcdefghijklmnopqrstuvwxyz \'?'


def phi(n):
    """
        Функция Эйлера
    """
    amount = 0
    for k in range(1, n + 1):
        if gcd(n, k) == 1:
            amount += 1
    return amount


def solve(a, b, m):
    """
        Решение сравнения a*x = b mod m
    """
    d = gcd(a, m)
    if b % d:
        return
    if b > m:
        b = b % m

    if d != 1:
        a, b, m = a // d, b // d, m // d
    result = pow(a, phi(m) - 1) * b
    result %= m
    return result


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.mainWidget = QWidget()
        self.setCentralWidget(self.mainWidget)
        self.source_text_widget = QLineEdit()
        self.public_key_widget = QLineEdit()
        self.result_widget = QLineEdit()
        self.B = None
        self.w = None
        self.q = None
        self.r = None
        self.input_text = None
        self.initUI()

    def initUI(self):
        self.setWindowTitle("ИЗ 4: задача 1")
        grid = QGridLayout()

        self.source_text_widget.setMinimumWidth(300)
        self.public_key_widget.setReadOnly(True)
        self.result_widget.setReadOnly(True)

        button_encrypt = QPushButton("Шифровать")
        button_decrypt = QPushButton("Расшифровать")
        button_encrypt.clicked.connect(self.go_encrypt)
        button_decrypt.clicked.connect(self.go_decrypt)

        buttons = QWidget()
        buttons_layout = QHBoxLayout(buttons)
        buttons_layout.addWidget(button_encrypt)
        buttons_layout.addWidget(button_decrypt)

        self.mainWidget.setLayout(grid)
        grid.addWidget(QLabel("Исходный текст:"), 0, 0)
        grid.addWidget(self.source_text_widget, 0, 1)
        grid.addWidget(QLabel("Открытый ключ:"), 1, 0)
        grid.addWidget(self.public_key_widget, 1, 1)
        grid.addWidget(buttons, 4, 0, 1, 2)
        grid.addWidget(QLabel("Результат:"), 3, 0)
        grid.addWidget(self.result_widget, 3, 1)

    @staticmethod
    def generate_keys(n):
        """
            всё по обозначениям в wiki https://u.to/D3HbFw
            функция генерирует открытый ключ - B и закрытый ключ (w, q, r)
            n - длина ключа
            w - список чисел длины n; сверхрастущий вектор
            q - число, большее суммы w
            r - число, взаимнопростое с q; 1 < r < q
            B - список чисел, являющийся открытым ключом
        """
        w = [randint(1, 10)]
        multiplier = randint(2, 4)
        for i in range(n - 1):
            w += [randint(sum(w) + 1, multiplier * sum(w))]

        q = randint(sum(w), multiplier * sum(w))
        r = randint(1, q)

        while gcd(q, r) != 1:
            r = randint(1, q)
        B = [(w[i] * r) % q for i in range(len(w))]

        return B, w, q, r

    def go_encrypt(self):
        """
            процедура шифрования
            делит текст на биграммы и шифрует каждую из них
        """
        self.input_text = self.source_text_widget.text().lower()
        if not self.input_text or any([item not in alphabet for item in self.input_text]):
            return
        self.B, self.w, self.q, self.r = self.generate_keys(10)
        bigrams = [self.input_text[i:i + 2] for i in range(0, len(self.input_text), 2)]
        if len(bigrams[-1]) == 1:
            bigrams[-1] = bigrams[-1] + " "
        result = []
        for bigram in bigrams:
            n1, n2 = alphabet.find(bigram[0]), alphabet.find(bigram[1])
            m = self.binarize(n1, n2)
            result.append(self.encrypt_block(m, self.B))
        self.result_widget.setText(', '.join([str(i) for i in result]))
        with open('private_key.json', 'w', encoding='utf-8') as json:
            dump({
                'q': self.q,
                'r': self.r
            }, json, indent=4)
        with open('public_key.json', 'w', encoding='utf-8') as json:
            dump({
                'b': self.B
            }, json, indent=4)

        self.public_key_widget.setText(', '.join(str(item) for item in self.B))

    def go_decrypt(self):
        """
            если не сохранены ключи (если не было шифрования в данном сеансе) читает их из файла формата
            {
                "q": int,
                "r": int
            }
            процедура создаёт сверхрастущий вектор и расшифровывает исходный текст
        """
        self.input_text = self.source_text_widget.text().lower()
        if not self.input_text or any([item not in "0123456789, " for item in self.input_text]):
            return
        if not self.B:
            self.read_from_json()
        text_to_decrypt = self.input_text.split(', ')
        text_to_decrypt = [int(item) for item in text_to_decrypt]
        decrypted = ''
        for number in text_to_decrypt:
            p = self.decrypt_block(number, self.w, self.r, self.q)
            l1 = alphabet[int(''.join(str(i) for i in p[:5]), 2)]
            l2 = alphabet[int(''.join(str(i) for i in p[5:]), 2)]
            decrypted += l1 + l2
        self.result_widget.setText(decrypted)

    def decrypt_block(self, c, w, r, q):
        """
        :param c: зашифрованное сообщение
        :params w, q, r: закрытый ключ
        :return:
        """
        plaintext = [0] * len(w)
        ctext = (c * self.modinv(r, q)) % q
        while ctext > 0:
            y = max(x for x in w if x <= ctext)
            ctext -= y
            plaintext[w.index(y)] = 1
        return plaintext

    def encrypt_block(self, m, B):
        """
            шифрование блока m по открытому ключу B
        """
        return sum([m[i] * B[i] for i in range(len(m))]) % self.q

    def read_from_json(self):
        """
            получение данных из файла
        """
        with open("public_key.json", encoding='utf-8') as json:
            self.B = load(json)["b"]

        with open("private_key.json", encoding='utf-8') as json_file:
            data_from_json = load(json_file)
            try:
                self.q = data_from_json["q"]
                self.r = data_from_json["r"]
                self.w = [solve(self.r, bi, self.q) for bi in self.B]
                self.public_key_widget.setText(', '.join(str(item) for item in self.B))
            except KeyError:
                self.get_private_key()

    @staticmethod
    def binarize(n1, n2):
        """
            переводит n1 и n2 в их бинарную форму длины 5
            n1, n2 < 32
            :return список длины 10, последовательно записанных n1, n2 в двоичной форме
        """
        lft = [int(i) for i in "{0:b}".format(n1)]
        rgh = [int(i) for i in "{0:b}".format(n2)]
        lft = [0 for _ in range(5 - len(lft))] + lft
        rgh = [0 for _ in range(5 - len(rgh))] + rgh
        return lft + rgh

    def modinv(self, a, m):
        """
        :return: мультипликативное обратное a по модулю m
        """
        g, x, y = self.egcd(a, m)
        if g != 1:
            raise Exception('modular inverse does not exist')
        else:
            return x % m

    def egcd(self, a, b):
        """
            расширенный алгоритм Эвклида
        """
        if a == 0:
            return b, 0, 1
        else:
            g, y, x = self.egcd(b % a, a)
            return g, x - (b // a) * y, y


if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = MainWindow()
    ex.show()
    sys.exit(app.exec_())
