from scipy.special import gammaincc
from math import fsum


def longest_run_ones_in_a_block_test(input):
    n = len(input)
    if n < 128:
        return 0.0, False
    if n < 6272:
        M = 8
        v = [0 for _ in range(4)]
        k = 3
        N = 16
        pi = [0.2148, 0.3672, 0.2305, 0.1875]
    elif n < 750000:
        M = 128
        v = [0 for _ in range(6)]
        k = 5
        N = 49
        pi = [0.1174, 0.2430, 0.2493, 0.1752, 0.1027, 0.1124]
    else:
        M = 10 ** 4
        v = [0 for _ in range(7)]
        k = 6
        N = 75
        pi = [0.0882, 0.2092, 0.2483, 0.1933, 0.1208, 0.0675, 0.0727]
    blocks = [input[i * M:((i + 1) * M)] for i in range(n // M)]

    for block in blocks:
        mx = 0
        current = 0
        i = 0
        while i != M:
            if block[i] == 1:
                current += 1
                i += 1
                continue
            i += 1
            if current > mx:
                mx = current
            current = 0
        if current > mx:
            mx = current
        if M == 8:
            if mx <= 1:
                v[0] += 1
            elif mx == 2:
                v[1] += 1
            elif mx == 3:
                v[2] += 1
            else:
                v[3] += 1
        elif M == 128:
            if mx <= 4:
                v[0] += 1
            elif mx == 5:
                v[1] += 1
            elif mx == 6:
                v[2] += 1
            elif mx == 7:
                v[3] += 1
            elif mx == 8:
                v[4] += 1
            else:
                v[5] += 1
        else:
            if mx <= 10:
                v[0] += 1
            elif mx == 11:
                v[1] += 1
            elif mx == 12:
                v[2] += 1
            elif mx == 13:
                v[3] += 1
            elif mx == 14:
                v[4] += 1
            elif mx == 15:
                v[5] += 1
            else:
                v[6] += 1

    x_2 = fsum([(pow(v[i] - N * pi[i], 2)) / (N * pi[i]) for i in range(k + 1)])
    p = gammaincc(float(k) / 2, x_2 / 2)
    return p, p >= 0.01
