from math import fsum, floor
from scipy.special import gammaincc


def frequency_test_within_a_block(input, M=32):
    n = len(input)
    if n < 100:
        # Too little data for test. Input of length at least 100 bits required
        return [0.0, 0.0, False]

    N = int(floor(n / M))

    proportions = []

    for i in range(N):
        block = input[i * M:((i + 1) * M)]
        proportions.append(float(sum(block)) / M)

    x_2 = 4 * M * fsum([pow(p_i - 0.5, 2) for p_i in proportions])
    p = gammaincc(N / 2, x_2 / 2)
    return p, p >= 0.01
