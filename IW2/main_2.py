import numpy as np
from monobit_test import monobit_test
from frequency_test_within_a_block import frequency_test_within_a_block
from runs_test import runs_test
from longest_run_ones_in_a_block_test import longest_run_ones_in_a_block_test
from dft_test import dft_test

"""
    https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-22r1a.pdf
    page 23
"""

psp = np.random.randint(2, size=10000)

print(monobit_test(psp))
print(frequency_test_within_a_block(psp, 30))
print(runs_test(psp))
print(longest_run_ones_in_a_block_test(psp))
print(dft_test(psp))
