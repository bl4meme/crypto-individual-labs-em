import numpy as np
from math import sqrt, erfc, log


def dft_test(input):
    n = len(input)
    s = np.fft.fft(np.array([-1 if i == 0 else 1 for i in range(n)]))
    M = abs(s)[:n // 2]
    T = sqrt(log(1.0 / 0.05) * n)
    N0 = 0.95 * n / 2.0

    N1 = sum([1 for i in M if i < T])
    d = (N1 - N0) / sqrt((n * 0.95 * 0.05) / 4)
    p = erfc(abs(d) / sqrt(2))
    return p, p >= 0.01
