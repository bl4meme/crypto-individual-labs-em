from math import sqrt, erfc


def monobit_test(input):
    ones = sum(input)  # number of ones
    zeroes = len(input) - ones  # number of zeros
    s = ones - zeroes
    s_obs = float(abs(s)) / sqrt(len(input))
    p = erfc(s_obs / sqrt(2))
    return p, p >= 0.01
