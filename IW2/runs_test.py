from math import sqrt, erfc
from monobit_test import monobit_test


def runs_test(input):
    n = len(input)
    pi = sum(input) / n
    tau = 2.0 / sqrt(n)

    if not (monobit_test(input)[1]) or abs(pi - 0.5) >= tau:
        return 0.0, False

    vn = 1
    for i in range(n - 1):
        if input[i] != input[i + 1]:
            vn += 1
    p = erfc((abs(vn - 2 * n * pi * (1 - pi))) / (2 * pi * (1 - pi) * sqrt(2 * n)))
    return p, p >= 0.01
